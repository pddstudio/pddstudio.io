# `Sinusbot` Docker Image

This image is a redestribution of the `sinusbot/docker` docker image.

## Notes

This container mounts two volumes upon boot:

  - ./data
  - ./scripts

