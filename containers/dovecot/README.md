
### [Dovecot](https://dovecot.org) Docker Image

> An open source IMAP/POP3 email server based upon [instrumentisto/dovecot-docker-image](https://github.com/instrumentisto/dovecot-docker-image)
