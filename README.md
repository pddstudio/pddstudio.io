# Personal Stack for DigitalOcean Droplet 

Deployed using docker-compose.

[pddstudio.io Website](pddstudio.io)

## License

[MIT Licensed](./LICENSE)

```
 (c) 2018 - MIT License - Patrick Jung
```
