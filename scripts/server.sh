#!/usr/bin/env bash

source ./scripts/helpers/internal/_deployment_utility_functions.sh

case "$1" in
  start | up)
    echo "Not available yet (up)"
    ;;
  down)
    shift;
    _compose_stack_stop $*
    ;;
  restart)
    echo "=> Not available yet (restart)"
    ;;
  pull-restart)
    echo "=> Not available yet (pull-restart)"
    ;;
  ps | status)
    echo "=> Not available yet (ps/status)"
    ;;
  logs)
    echo "=> Not available yet (logs)"
    ;;
  *)
    echo "Usage: $0 {start [up] | down | restart | pull-restart | ps [status] | logs}"
    exit 0
    ;;
esac

if [ $? -eq 0 ]; then
  echo "=> Command executed successfully!"
else
  echo "=> An Error occurred during command execution!"
fi
