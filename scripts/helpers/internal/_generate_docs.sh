#!/usr/bin/env bash
###
# File: _deployment_utility_functions.sh
# Author: Patrick Jung
# License: (c) 2018 - MIT License - Patrick Jung <patrick.pddstudio@gmail.com>
# Description:
# This file contains some utility functions used by the server.sh script in the root of this repository.
# The helper functions in this file should not be called directly - they're sourced by the main server.sh script.
# These functions are intended to be used on my deployed droplet only,
# there is no guarantee for these functions to execute successfully on any other setup. (as server.sh in general)
###

function _clean_docs () {
  local BUILD_DIR=$(pwd)/docs/build/
  if [ -d "$BUILD_DIR" ]; then
    echo "=> Cleaning documentation build output"
    rm -rf $BUILD_DIR
    return $?
  fi
  return 0
}

function _build_docs() {
  local BUILD_DIR=$(pwd)/docs/build/
  mkdocs build --config-file docs/mkdocs.yml &> /dev/null
  if [ -d "$BUILD_DIR" ]; then
    echo "=> Documentation successfully created!"
    return 0;
  else
    echo "=> An error occurred while building documentation. Are all requirements satisfied?"
    return 1;
  fi
}

function _install_docs_requirements () {
  local REQ_FILE=$(pwd)/docs/requirements.txt
  echo "=> Installing requirements using pip..."
  pip install --no-cache-dir -r $REQ_FILE &> /dev/null
  return $?
}
