#!/usr/bin/env bash

print_toolchain_info() {
  echo "==== BEGIN TOOLCHAIN VERSIONS ===="
  echo "Using Docker Version: $(docker version)"
  echo "Using docker-compose Version: $(docker-compose version)"
  echo "Node Version: $(node -v)"
  echo "NPM Version: $(npm -v)"
  echo "==== END TOOLCHAIN VERSIONS ===="
  return 0
}

build_and_push_docker_images() {
  local NODE_VERS=$(node --version)
  local BUILD_SCRIPT="$(pwd)/scripts/docker/buildAll.js"
  echo "Building with node version: $NODE_VERS"
  echo "===="
  echo "Working Directory is: $(pwd)"
  echo "Excpecting build script at '$BUILD_SCRIPT'"
  echo "===="
  $BUILD_SCRIPT
  return $?
}

install_nvm_node () {
  curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
  export NVM_DIR="$HOME/.nvm"
  [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
  [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
  command -v nvm &> /dev/null
  if [ $? -eq 0 ]; then
    # nvm setup complete: movw on with install latest node  * npm
    nvm install --latest-npm node &2> /dev/null
    return $?
  else
    # basic errorhandling
    echo "ERROR: An error occurred during nvm installation!"
    return 1;
  fi
}
