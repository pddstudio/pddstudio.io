#!/usr/bin/env bash
###
# File: _deployment_utility_functions.sh
# Author: Patrick Jung
# License: (c) 2018 - MIT License - Patrick Jung <patrick.pddstudio@gmail.com>
# Description:
# This file contains some utility functions used by the server.sh script in the root of this repository.
# The helper functions in this file should not be called directly - they're sourced by the main server.sh script.
# These functions are intended to be used on my deployed droplet only, 
# there is no guarantee for these functions to execute successfully on any other setup. (as server.sh in general)
###

###
# Function to remove any existing server.log file inside the current working directory.
# This will remove logs only if they are present and return the exit code of the rm command,
# otherwise 0 is returned.
###
function _rm_logs_if_exist() {
  local LOG_FILE=$(pwd)/server.log
  if [ -e $LOG_FILE ]; then
    echo "=> Removing existing logs..."
    rm $LOG_FILE
    return $?
  fi
  return 0
}

###
# Helper function to stop the currently running compose stack.
# Returns the output of the wrapped docker-compose down command.
# Additional information/logs are written to the server.log file in the root of this repository.
###
function _compose_stack_stop() {
  local LOG_FILE=$(pwd)/server.log
  local FORCE=0
  while [[ "$1" =~ ^- && ! "$1" == "--" ]]; do
    case $1 in
      -f | --full )
        FORCE=1
        ;;
    esac;
    shift; 
  done
  if [[ "$1" == '--' ]]; then
    shift;
  fi
  echo "=> Stopping compose stack..."
  if [ $FORCE -eq 1 ]; then
    echo "  => FORCE REMOVAL: ON"
    docker-compose down -v --remove-orphans --rmi all &> $LOG_FILE
    return $?
  else
    echo "  => FORCE REMOVAL: OFF"
    docker-compose down &> $LOG_FILE
    return $?
  fi
  return $?
}
