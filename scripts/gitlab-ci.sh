#!/usr/bin/env bash

source ./scripts/helpers/optparse/optparse.bash

optparse.define short=r long=run desc="Specify the run action to execute" variable=target_cmd default=help
optparse.define short=d long=debug desc="Enable debug output" variable=debug_mode value=true default=false
optparse.define short=x long=prepare-env desc="Prepare environment before execution" variable=prep_env value=true default=false

source $( optparse.build )
source ./scripts/helpers/internal/_ci_utils.sh

evaluate_target_command() {
  case "$1" in
    build)
      build_and_push_docker_images
      local exit_code=$?
      echo "Build command complete. Exiting with statuscode: $exit_code !"
      exit $exit_code
      ;;
    install)
      install_nvm_node
      local exit_code=$?
      echo "Install command complete. Exiting with statuscode: $exit_code !"
      exit $exit_code
      ;;
    print-debug | debug)
      print_toolchain_info
      exit $?
      ;;
    help | *)
      echo "Usage: $0 { build | clean-build | print-debug [debug] }"
      exit 0
      ;;
  esac
}

prepare_env() {
  . $NVM_DIR/nvm.sh
}

if [ "$target_cmd" == "" ]; then
  echo "ERROR: Target command not specified"
  exit 1
else
  if [ "$prep_env" = "true" ]; then prepare_env; fi
  evaluate_target_command "$target_cmd"
fi

