#!/usr/bin/env node

var process = require('process');
var path = require('path');
var fs = require('fs');
var cproc = require('child_process');
var os = require('os');

const logDir = path.resolve(os.tmpdir());

async function writeLogs(filename, data) {
  try {
    const destFileName = path.resolve(logDir, filename);
    await fs.writeFileSync(destFileName, data.join('\n'));
    console.log('Build & push output written to: ' + destFileName);
  } catch (error) {
    // ignore
  }
}

async function buildImage(imageInfo) {
  // const buildCommand = 'pushd ' + imageInfo.directory + ' && docker build -t ' + imageInfo.tag + ' . && popd'
  const buildCommand = 'docker build --pull -t ' + imageInfo.tag + ' .';

  let cmdout = [];

  try {
    const result = await cproc.execSync(buildCommand, { cwd: imageInfo.directory, encoding: 'utf-8' });
    cmdout.push(result);
    const pushResult = await pushImage(imageInfo);
    if (pushResult) {
      cmdout.push(pushResult);
    }
    console.log('  => Finished building docker image for ' + imageInfo.dirname);
  } catch (error) {
    console.error(`  => An error occurred while building docker image: ${error.message}`);
  }
  // write loogs
  try {
    const time = new Date(Date.now()).toISOString();
    await writeLogs(`${imageInfo.dirname}-build-${time}.log`, cmdout);
  } catch (error) {
    // ignore
  }
}

async function pushImage(imageInfo) {
  const pushCommand = 'docker push ' + imageInfo.tag;
  try {
    const result = await cproc.execSync(pushCommand, { encoding: 'utf-8'});
    console.log('  => Pushed successful for image ' + imageInfo.tag);
    return result;
  } catch (error) {
    console.error('  => Unable to push image ' + imageInfo.tag + ': ' + error.message);
    return undefined;
  }
}

async function findDockerfiles() {
  const targetDir = path.join(__dirname, '../../containers');
  console.log('target dir: ' + targetDir);
  const dirs = await fs.readdirSync(targetDir);
  dirs.forEach(dir => {
    const td = path.join(targetDir, dir);
    const dockerfile = path.join(targetDir, 'Dockerfile');
    const tag = 'registry.gitlab.com/pddstudio/pddstudio.io/' + dir + ':latest'
    const buildInfo = { dockerfile: dockerfile, tag: tag, directory: td, dirname: dir }
    buildImage(buildInfo);
  });
}

async function buildDockerImages() {
  console.log('Building Dockerfiles for container stack, this might take a while...');
  await findDockerfiles();
}

buildDockerImages().then(() => {
  console.log('Build job finished execution!');
  process.exit(0);
}).catch(error => {
  console.error(error);
  process.exit(1);
});
