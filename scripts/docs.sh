#!/usr/bin/env bash

source ./scripts/helpers/internal/_generate_docs.sh

case "$1" in
  install | requirements)
    _install_docs_requirements
    ;;
  generate)
    _build_docs
    ;;
  clean)
    _clean_docs
    ;;
  *)
    echo "Usage: $0 {install [requirements] | generate | clean}"
    exit 0
    ;;
esac

if [ $? -eq 0 ]; then
  echo "=> Command executed successfully!"
else
  echo "=> An Error occurred during command execution!"
fi
